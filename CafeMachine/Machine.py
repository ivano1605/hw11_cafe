from CafeMachine.Cashback import Cashback
from CafeMachine.User import User
from Beverages.drinks import *

class Cafemachine:
    def __init__(self, user: User, cash: int, beverage):
        self.user = user
        self.cash = cash
        self.beverage = beverage

    def beverage_price(self):
        beverage = self.beverage
        beverage += '.cost()'
        price = eval(beverage)
        discount = 0
        if self.user.cashback >=50<100:
            discount = Cashback.from_50_to_100.value
        if self.user.cashback >=100<250:
            discount = Cashback.from_100_to_250.value
        if self.user.cashback >=250<300:
            discount = Cashback.from_250_to_300.value
        if self.user.cashback >=300:
            discount = Cashback.from_300.value
        return price - (price * discount)

    def rest(self):
        rest = self.cash - self.beverage_price()
        return rest

    def make_coffee(self):
        try:
            assert(self.rest() >= 0)
        except:
            return 'Not enough money for this beverage'

        else:
            return 'Enjoy your drink'

    def add_cashback(self):
        if self.rest() >= 0:
            if self.beverage_price() < 50:
                self.user.cashback += self.beverage_price()

    def show_cashback(self):
        return self.user.cashback
