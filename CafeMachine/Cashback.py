from enum import Enum


class Cashback(Enum):
    from_50_to_100 = 0.05
    from_100_to_250 = 0.15
    from_250_to_300 = 0.2
    from_300 = 0.3


