from Beverages.beverage import *


class HouseBlend(Beverage):

    def cost(self):
        cost = 4
        return cost + self.adds_price


class DarkRoast(Beverage):

    def cost(self):
        cost = 4.50
        return cost + self.adds_price


class Decaf(Beverage):

    def cost(self):
        cost = 5.50
        return cost + self.adds_price


class Espresso(Beverage):

    def cost(self):
        cost = 2.50
        return cost + self.adds_price


mocco_bland = HouseBlend('tasty roasted cofe', 'mocco')
espresso = Espresso('Strong')
decaf = Decaf('No cofein')
darc_roast_mocco = DarkRoast('tasty roasted cofe', 'mocco')

