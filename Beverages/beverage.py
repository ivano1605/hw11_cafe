from abc import ABCMeta, abstractmethod
from Beverages.adds import *


@mocco
@sugar
@milk
class Beverage(metaclass=ABCMeta):
    def __init__(self, description, adds=None):
        self.description = description
        self.adds = adds if adds else list()

    @abstractmethod
    def cost(self):
        pass

    def list_of_adds(self):
        return self.adds

    @property
    def adds_price(self):
        price = 0
        if 'milk' in self.adds:
            price += self.milk()
        if 'sugar' in self.adds:
            price += self.sugar()
        if 'mocco' in self.adds:
            price += self.mocco()
        return price

    def get_description(self):
        return self.description
