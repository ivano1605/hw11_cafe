def sugar(cls):
    def wrapper():
        def sugar(self):
            return 1
        cls.sugar = sugar
        return cls

    return wrapper()


def mocco(cls):
    def wrapper():
        def mocco(self):
            return 3

        cls.mocco = mocco
        return cls

    return wrapper()


def milk(cls):
    def wrapper():
        def milk(self):
            return 2

        cls.milk = milk
        return cls

    return wrapper()
